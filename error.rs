//! Failure state for constructing [`crate::ArrayOfBase`]

/// Failure state for constructing [`crate::ArrayOfBase`]
///
/// # Used in
/// - [`crate::ArrayOfBase::try_new`],
/// - [`crate::ArrayOfBase::try_from_vec_exact`]
/// - [`crate::ArrayOfBase::try_from_vec_pad`]
#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    /// Supplied array is larger then specified `N`
    Oversized,
    /// Supplied array is smaller then specified `N`
    ///
    /// Note: Not returned in [`crate::ArrayOfBase::try_from_vec_pad`] as any non-zero
    /// size smaller than `N` is accepted
    Undersized,
    /// Supplied array is empty
    Empty,
    /// Supplied array contains a value equal-to or larger than `B`
    InvalidValue,
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:?}")
    }
}
