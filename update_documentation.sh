# Update CargoDocs from source code
cargo doc --all-features --workspace

# Update README.md from source code
cargo readme > README.md
