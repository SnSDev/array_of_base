//! An array of specified length `N` of u8 values from 0 to `B`-1
//!
//! # Problem
//!
//! Rust allows storing arrays on the stack of numbers, but no way to ensure
//! that said numbers are in a specified range
//!
//! # Solution
//!
//! Wrapper around array that verifies all numbers when constructed

#![warn(missing_docs)]
#![warn(clippy::pedantic)]
#![warn(clippy::str_to_string)]
#![warn(clippy::string_to_string)]

mod array_of_base;
mod error;

pub use crate::array_of_base::ArrayOfBase;
pub use error::Error;

/// Alias for fixed length array of hexadecimal values
///
/// See [`ArrayOfBase`]
///
/// # Example
///
/// ```
/// # use array_of_base::ArrayOfHex;
/// // GIVEN
/// let valid_array: [u8; 16] = core::array::from_fn(|i| i as u8);
/// let invalid_array = [16];
///
/// // WHEN
/// let valid_result = ArrayOfHex::<16>::try_new(valid_array);
/// let invalid_result = ArrayOfHex::<1>::try_new(invalid_array);
///
/// // THEN
/// assert!(valid_result.is_ok());
/// assert!(invalid_result.is_err());
/// ```
pub type ArrayOfHex<const N: usize> = ArrayOfBase<N, 16>;

/// Alias for fixed length array of decimal values
///
/// See [`ArrayOfBase`]
///
/// # Example
///
/// ```
/// # use array_of_base::ArrayOfDec;
/// // GIVEN
/// let valid_array: [u8; 10] = core::array::from_fn(|i| i as u8);
/// let invalid_array = [10];
///
/// // WHEN
/// let valid_result = ArrayOfDec::<10>::try_new(valid_array);
/// let invalid_result = ArrayOfDec::<1>::try_new(invalid_array);
///
/// // THEN
/// assert!(valid_result.is_ok());
/// assert!(invalid_result.is_err());
/// ```
pub type ArrayOfDec<const N: usize> = ArrayOfBase<N, 10>;
