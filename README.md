# array_of_base

An array of specified length `N` of u8 values from 0 to `B`-1

## Problem

Rust allows storing arrays on the stack of numbers, but no way to ensure
that said numbers are in a specified range

## Solution

Wrapper around array that verifies all numbers when constructed

License: MIT
